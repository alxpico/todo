package android.rico.todo;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoPostAdapter extends RecyclerView.Adapter<TodoPostHolder>{

    private ArrayList<TodoItem> todoPost;
    private ActivityCallback activityCallback;

    public TodoPostAdapter(ActivityCallback activityCallback, ArrayList<TodoItem> todoPost) {

        this.todoPost = todoPost;

        this.activityCallback = activityCallback;
    }

    @Override
    public TodoPostHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        return new TodoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Uri redditUri = Uri.parse(todoPost.get(position).url);
                //activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(todoPost.get(position).title);

    }

    @Override
    public int getItemCount() {
        return todoPost.size();
    }
}
