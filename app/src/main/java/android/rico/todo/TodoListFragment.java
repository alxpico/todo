package android.rico.todo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoListFragment extends Fragment{

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private TodoActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (TodoActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        TodoItem i1 = new TodoItem("date","time","min","sec");
        TodoItem i2 = new TodoItem("time","e","n","e");
        TodoItem i3 = new TodoItem("min","t","m","s");
        TodoItem i4 = new TodoItem("sec","e","mn","ec");

        activity.TodoItem.add(i1);
        activity.TodoItem.add(i2);
        activity.TodoItem.add(i3);
        activity.TodoItem.add(i4);


        TodoPostAdapter adapter = new TodoPostAdapter(activityCallback, activity.TodoItem);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
