package android.rico.todo;

public class TodoItem {
    public String title;
    public String dateAdd;
    public String dateDue;
    public String catagory;

    public TodoItem(String title, String dateAdd, String dateDue, String catagory){
        this.title = title;
        this.dateAdd = dateAdd;
        this.dateDue = dateDue;
        this.catagory = catagory;

    }

}
