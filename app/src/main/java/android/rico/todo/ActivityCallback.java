package android.rico.todo;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
