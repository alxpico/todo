package android.rico.todo;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.app.Fragment;

import java.util.ArrayList;


public class TodoActivity extends SingleFragmentActivity implements ActivityCallback{
    MediaPlayer mediaPlayer;

    public ArrayList<TodoItem> TodoItem = new ArrayList<TodoItem>();
    public int currentItem;

    @Override
    protected Fragment createFragment() {
        mediaPlayer = MediaPlayer.create(this, R.raw.play);
        return new TodoListFragment();
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new Fragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}